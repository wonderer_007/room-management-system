class WelcomeController < ApplicationController

  def index
  end

  def search

    date = DateTime.parse params[:query]
    data = {}
    data[:date] = date.to_i
    @date = date.to_date

    response = RestClient.post 'https://challenges.1aim.com/roombooking/getrooms', data.to_json, :content_type => 'application/json'

    if response.code == 200
      @resp = JSON.parse response.body
    else
      flash[:error] = 'Please try again !!!'
    end

  end

end