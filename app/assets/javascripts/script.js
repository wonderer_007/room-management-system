
$(function () {

  debugger;
  $("[data-toggle=tooltip]").tooltip();
  $( "#end_time" ).prop( "disabled", true );

  var participant = {};
  var timeline    = {};

  for(var i=0;i<slider_count;i++) {
    $('.slider_'+i).tinycarousel({
      animate: true,
      animationTime: 10
    });      
  }
  
  $(document).on("click", ".submit_participant", function(){

    if(!$("#email").valid() || !$("#name").valid() || !$("#phone").valid())
      return false;

    if(!validatePhone($("#phone").val())) {
      swal({ title: "Invalid Phone Number", html: true });
      return false;
    }

    if(participant[$("#email").val()] == undefined) {

      participant[$("#email").val()] =  { email: $("#email").val(),
                                          name: $("#name").val(),
                                          phone:$("#phone").val()
                                        }
      console.log($("#email").val());
      console.log($("#name").val());
      console.log($("#phone").val());
      $('#myModalNorm').modal('hide');

    } else {
      swal({ title: "Participant Already Exists",   
            text: "participant with email "+$("#email").val()+ " already exists",   
            html: true });
    }


    $("#email").val('');
    $("#name").val('');
    $("#phone").val('');

    var html = '<table class="table table-bordered"><h3>Participants</h3><thead><tr><th>Email</th><th>Name</th><th>Phone</th><th>Action</th></tr></thead><tbody>';
    for (var part in participant) {

      html += "<tr>";
      html += "<td>"+ participant[part]["email"] + "</td>";
      html += "<td>"+ participant[part]["name"] + "</td>";
      html += "<td>"+ participant[part]["phone"] + "</td>";
      html += "<td><a href='#' class='remove_participant' data-participant-email='"+participant[part]["email"]+"'>remove</a></td>";
      html += "</tr>";
    }
      html += "</table>";

    $(".participant_detail").html(html);

    return false;
  });

  $('#datetimepicker1').datepicker({
    format: "yyyy/mm/dd"
  }).on('changeDate', function(e){
    $(this).datepicker('hide');
    $("#search").submit();
  });

  $( ".timeline" ).mouseover(function() {
    $(this).html("<span>"+$(this).data("value").replace(/ /g,'')+"</span>");
  }).mouseout(function() {
    $(this).html('');
  });



  $(".book_now").on("click", function() {

    $("#room_booking").remove();
    participant = {};

    var room_index = $(this).data("room-index");

    var options = "";
    $("#timeline_"+room_index+" div").each(function () {

      var value = $(this).data("value");
      var index = $(this).data("index");        

      if($(this).data("avail"))
        options += "<option data-index='"+index+"' value='"+value+"' > "+ value +"</option>";
      else
        options += "<option value='"+value+"' disabled> "+ value +"</option>";

    });

    var html = '<div class="col-sm-5 col-sm-offset-3"><div class="form-group"><select class="form-control start_time" data-room-index="'+room_index+'" required><option value="" selected disabled>Start Time</option>';

      html += options + '</select></div></div><div class="col-sm-5 col-sm-offset-3"><div class="form-group" id="test"><select class="form-control end_time" required><option value="" selected disabled>End Time</option></select></div></div>';

    $("#room_"+room_index).after('<form id="room_booking"><div class="row booking"><div class="col-sm-5 col-sm-offset-3"><h3>Booking Details</h3><small style="float:right">All fields required</small> <div class="form-group"> <input class="form-control" id="title" placeholder="Event Title" type="text" required> </div> </div> <div class="col-sm-5 col-sm-offset-3"> <div class="form-group"> <textarea class="form-control" id="description" placeholder="Event Description" required></textarea> </div> </div> '+html+'<div class="col-sm-5 col-sm-offset-3 participant_detail"></div><div class="col-sm-5 col-sm-offset-3"> <div class="form-group"> <button type="button" class="btn btn-custom btn-xs" id="add_participant" data-target="#myModalNorm">Add Participant</button> </div> </div><div class="col-sm-5 col-sm-offset-4"> <div class="form-group"> <button type="button" class="btn btn-custom" id="confirm_booking" data-room-index="'+room_index+'">Confirm Booking</button> </div> </div> </div></form>');

    $("#room_booking").hide().slideDown("slow");
    if(timeline[room_index] != undefined) {

      if(timeline[room_index]['start'] != undefined) {

        var obj = $("#timeline_"+room_index+" div").filter('[data-index="'+timeline[room_index]['start']+'"]');
        $(".start_time").val(obj.data("value"));

        $('.end_time').find("option").remove();
        $('.end_time')
           .append($("<option></option>")
           .attr("value","")
           .attr("disabled", true)
           .text("End Time"));

        var count =0;
        var start_time_index = $(".start_time :selected").data("index"); 

        $("#timeline_"+room_index+" div").each(function () {

          var value = $(this).data("value");
          var index = $(this).data("index");

          if($(this).data("avail") == false || count < start_time_index)
            $('.end_time')
               .append($("<option></option>")
               .attr("value",value)
               .attr("disabled", true)
               .attr("data-index",index)
               .text(value));
          else
            $('.end_time')
              .append($("<option></option>")
              .attr("value",value)
              .attr("data-index",index)
              .text(value));

            count += 1;
        });

      }

      if(timeline[room_index]['end'] != undefined) {

        var obj = $("#timeline_"+room_index+" div").filter('[data-index="'+timeline[room_index]['end']+'"]');

        $('.end_time').find("option").remove();
        $('.end_time')
           .append($("<option></option>")
           .attr("value","")
           .attr("disabled", true)
           .text("End Time"));
        $("#timeline_"+room_index+" div").each(function () {

          var value = $(this).data("value");
          var index = $(this).data("index");        

          $('.end_time')
             .append($("<option></option>")
             .attr("value", value)
             .attr("data-index", index)
             .text(value));

        });

        $(".end_time").val(obj.data("value"));

      }

    }

  });


  $("body").on("click", "#add_participant", function() {
    $('#myModalNorm').modal('show');
  });


  $("body").on("click", ".remove_participant", function() {
    if(participant[$(this).data("participant-email")] != undefined) {
      $(this).parent().parent().remove();
      delete participant[$(this).data("participant-email")];
      swal("Deleted!", "Participant deleted successfully !", "success");
    }
    return false;
  });


  $("body").on("click", "#confirm_booking", function(){

    if( $("#title").valid() && $("#description").valid() 
      && $(".start_time").valid() && $(".end_time").valid() ) {

      var room_index = $(this).data("room-index");
      var room_name = $("#room_"+room_index+" h2").text();
      var hour = parseInt($(".start_time").val().split(":")[0].trim());
      var min  = parseInt($(".start_time").val().split(":")[1].trim());
      
      var start_time = new Date(date.split("-")[0], date.split("-")[1], date.split("-")[2],hour,min)/1000;

      hour  = parseInt($(".end_time").val().split(":")[0].trim());
      min   = parseInt($(".end_time").val().split(":")[1].trim());

      var end_time = new Date(date.split("-")[0], date.split("-")[1], date.split("-")[2],hour,min)/1000;

      var data = {};
        data['booking'] = {
          'date': new Date(date.split("-")[0], date.split("-")[1], date.split("-")[2]).getTime()/1000,
          'time_start': start_time,
          'time_end': end_time,
          'title': $("#title").val(),
          'description': $("#description").val(),
          'room': room_name
        };

        data["passes"] = [];
        for(var part in participant) {
          data["passes"].push({'name':participant[part]['name'], 'email':participant[part]['email'], 'number': participant[part]['phone']})
        }
   
    $.ajax({
      url: 'https://challenges.1aim.com/roombooking/sendpass',
      type: "POST",
      dataType: 'json',
      data: JSON.stringify(data),
      error: function(error) {
        sweetAlert("Oops...", "Something Went Wrong Please try again", "error");
      },
      success: function(data) {
        if(data.success == true) {
          $("#room_booking").remove();
          swal("Booking Confirmed", "", "success")
        }
        else {
          var error_msg = data.error.text.replace(".", " ").replace(".", " ").replace("[", " ").replace("]", " ").replace("-", " ");
            sweetAlert("Oops...", error_msg, "error");
        }
      },
    });
    
      swal({
        title: "Please wait",
        text: "",
        timer: 20000,
        showConfirmButton: false
      });

    }
  });

  $('body').on('click', '.timeline', function() {

    if($(this).data("avail") == false)
      return ;

    var number_of_clicks = $(this).parent().children().filter('[data-clicked=true]').length;

    if(number_of_clicks == 0 ) {

        $(this).css("background", "yellow");
        $(this).attr("data-clicked", true);
        timeline[$(this).parent().data("room-index")] = { 'start':$(this).data("index") };
        $(".start_time").val($(this).data("value"));
        $('.end_time').find("option").remove();
        $('.end_time')
           .append($("<option></option>")
           .attr("value","")
           .attr("disabled", true)
           .text("End Time"));

        var count =0;
        var start_time_index = $(this).data("index"); 

        $(this).parent().children(".timeline").each(function() {

          var value = $(this).data("value");
          var index = $(this).data("index");

          if($(this).data("avail") == false || count < start_time_index)
            $('.end_time')
               .append($("<option></option>")
               .attr("value",value)
               .attr("disabled", true)
               .attr("data-index",index)
               .text(value));
          else
            $('.end_time')
              .append($("<option></option>")
              .attr("value",value)
              .attr("data-index",index)
              .text(value));

            count += 1;
        });

    }
    else if(number_of_clicks == 1) {

      if($(this).data("clicked") == true) {

        $(this).css("background", "#00A0DB");
        $(this).attr("data-clicked", false);
        $(".start_time").val($(this).data(""));
        return;
      }


      var click1 = $(this).parent().children().filter('[data-clicked=true]').data("index");
      var obj1   = $(this).parent().children().filter('[data-clicked=true]');
      var click2 = $(this).data("index");
      $(this).attr("data-clicked", true);

      if(click1 < click2) {

        $(this).parent().children(".timeline").each(function(i, obj) {
          if(i>=click1 && i<= click2 && $(this).data("avail") == true) {
            $(this).css("background", "yellow");              
          }
        });

        timeline[$(this).parent().data("room-index")] = { 'end':click2, 'start': click1 };
        $(".start_time").val(obj1.data("value"));

        $('.end_time').find("option").remove();

        $('.end_time')
           .append($("<option></option>")
           .attr("value",$(this).data("value"))
           .attr('selected', true)
           .text($(this).data("value")));


      }
      else {

        $(this).parent().children(".timeline").each(function(i, obj) {
          if(i>=click2 && i<= click1  && $(this).data("avail") == true)
            $(this).css("background", "yellow");
        });

        timeline[$(this).parent().data("room-index")] = { 'start':click2, 'end':click1 };

        $(".start_time").val($(this).data("value"));
        $('.end_time').find("option").remove();
        $('.end_time')
           .append($("<option></option>")
           .attr("value",obj1.data("value"))
           .attr('selected', true)
           .text(obj1.data("value")));
      }
    }
    else {

      $(".start_time").val("");
      $('.end_time').find("option").remove();

      $('.end_time')
         .append($("<option></option>")
         .attr("value","")
         .attr("disabled", true)
         .text("End Time"));

      var count =0;
      var start_time_index = $(this).data("index");

      $(this).parent().children(".timeline").each(function(i, obj) {

        if($(this).data("avail"))
          $(this).css("background", "#00A0DB");
        else
          $(this).css("background", "red");

        delete timeline[$(this).parent().data("room-index")];
        $(this).attr("data-clicked", false);


      var value = $(this).data("value");
      var index = $(this).data("index");

      if($(this).data("avail") == false || count < start_time_index)
        $('.end_time')
           .append($("<option></option>")
           .attr("value",value)
           .attr("disabled", true)
           .attr("data-index",index)
           .text(value));
      else
        $('.end_time')
          .append($("<option></option>")
          .attr("value",value)
          .attr("data-index",index)
          .text(value));

        count += 1;

      });




        $(this).css("background", "yellow");
        $(this).attr("data-clicked", true);
        timeline[$(this).parent().data("room-index")] = { 'start':$(this).data("index") };
        $(".start_time").val($(this).data("value"));
    }
  });

  $('body').on('change', '.end_time', function() {

    var room_index  = $(".start_time").data("room-index");
    var start_index = $(".start_time option:selected").data('index');
    var end_index   = $(".end_time option:selected").data('index');

    $("#timeline_"+room_index+" div").each(function(i, obj) {

      if(i>=start_index && i<= end_index  && $(this).data("avail") == true)
        $(this).css("background", "yellow");

      if(i == end_index)
        $(this).attr("data-clicked", true);

    });

  });

  $('body').on('change', '.start_time', function() {

    var room_index    = $(this).data("room-index");
    var start_time    = $(".start_time").val();
    var option_index  = $(".start_time option:selected").data("index");

    var start_time_index = (start_time.split(":")[0].trim()-7)*4 + parseInt(start_time.split(":")[1].trim()/15);

    console.log("start time index " + start_time_index);
    var options = "";
    var count = 0;

    $('.end_time').find("option").remove();
    $('.end_time')
       .append($("<option></option>")
       .attr("value","")
       .attr("disabled", true)
       .text("End Time"));

    $("#timeline_"+room_index+" div").each(function () {

      var value = $(this).data("value");
      var index = $(this).data("index");

      if($(this).data("avail"))
        $(this).css("background", "#00A0DB");
      else
        $(this).css("background", "red");

      if($(this).data("index") == option_index) {
        $(this).css("background", "yellow");
        $(this).attr("data-clicked", true);

      }


      if($(this).data("avail") == false || count < start_time_index)
        $('.end_time')
           .append($("<option></option>")
           .attr("value",value)
           .attr("disabled", true)
           .attr("data-index",index)
           .text(value));
      else
        $('.end_time')
          .append($("<option></option>")
          .attr("value",value)
          .attr("data-index",index)
          .text(value));

        count += 1;
    });

  });

  $("#filter").keyup(function() {
    $("#room_booking").remove();
    $('.room').each(function(i, obj) {
      var room_name = $(this).find("h2").text().split(" ")[1];
      if(room_name.indexOf($("#filter").val()) > -1)
        $(this).css("display","block");
      else
        $(this).css("display","none");
    });

  });

  $(document).on("click", "#available_now", function() {

    $("#room_booking").remove();
    $("#filter").val('');
    var date = new Date();
    var hour = date.getHours();

    if($("#available_now").prop('checked')) {

      $('.room').each(function(i, obj) {
        var flag = $("#room_"+i+" .timeline").filter('[data-hour="'+hour+'"]');
        if(flag.length > 0 && flag.data("avail"))
          $(this).css("display","block");
        else
          $(this).css("display","none");
      });

    }
    else {
      $('.room').each(function(i, obj) {
          $(this).css("display","block");
      });
    }
  });
});

function validatePhone(txtPhone) {

  var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;

  if (filter.test(txtPhone) && txtPhone>=7)
    return true;
  else
    return false;
}