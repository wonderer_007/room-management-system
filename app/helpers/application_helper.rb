module ApplicationHelper

  def num_to_time(num)

    if num%4 == 0
      "#{(num/4)+7} : 00"
    else
      "#{(num/4)+7} : #{(num%4)*15}"
    end

  end

  def get_hour(num)
    (num/4)+7
  end
end